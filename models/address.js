// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;


// schema for the shipping address. required: true means it will raise exception otherwise
var addressSchema = new mongoose.Schema({
    _id:  {type: String, required: true},
    customer_id: {type: String, required: true},
    name: {type: String, required: true},
    address_1: {type: String, required: true},
    address_2: {type: String, required: true},
    city: {type: String, required: true},
    pincode: {type: String, required: true},
    state: {type: String, required: true}
});

// Return model
module.exports = restful.model('Address', addressSchema);