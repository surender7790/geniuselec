// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

//Schema definition
var orderSchema = new mongoose.Schema({
    _id:  {type: String, required: true},
    customer_id: {type: String, required: true},
    productCart: [new mongoose.Schema({
        product_id:{type: Array},
        product_qty:{type:Number}
    },{_id: false})],
    orderAmount : {type: Number, required: true},
    orderDate: {type: Date, required: true},
    processingDate: {type: Date, required: true},
    shippingDate: {type: Date, required: true},
    canceledDate: {type: Date, required: true},
    deliveryDate: {type: Date, required: true},
    orderStatus : {type: String, required: true}
});

// Return model
module.exports = restful.model('Order', orderSchema);
