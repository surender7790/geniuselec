// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

//Schema definition
var productSchema = new mongoose.Schema({
    _id:  {type: String, required: true},
    name: {type: String, required: true},
    description: {type: String, required: true},
    inStock: {type: String, required: true},
    inStockCount: {type: String, required: true},
    price: {type: String, required: true},
    image: {type: String, required:true}
//  dateAvailable: {type: Date, required: true},
});

// Return model
module.exports = restful.model('Product', productSchema);
