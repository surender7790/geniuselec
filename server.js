// Commands
// Start mongodb: sudo mongod --dbpath /data/db
// start node.js app: nodemon server.js
// Check POSTMAN for the API implementations

// Dependencies
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var path = require('path');

// MongoDB
mongoose.connect('mongodb://localhost/rest_test');

// Tried to connect to the MLAB servers for a mongo instance. but failed
//mongoose.connect('mongodb://arpitnarechania:<password>@ds033076.mlab.com:33076/rest_test_an');

// Express
var app = express();
// will help in parsing all the input params, body into json.
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// will route all requests to the /api endpoint to the /routes/api.js file
app.use('/api', require('./routes/api'));

// used to debug the /root path for referencing the baseURL
console.log(__dirname);

// very important piece of code. will serve the angularjs app located at app/index.html
var serveStatic = require('serve-static');
app.use(serveStatic(__dirname, {'index': ['app/index.html']}));

// Start server
app.listen(3000);
console.log('Listening on port 3000...');