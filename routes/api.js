// Dependencies
var express = require('express');
var router = express.Router();


// Models created
var Product = require('../models/product');
var Customer = require('../models/customer');
var Address = require('../models/address');
var Client = require('../models/client');
var Order = require('../models/order');

// update signUpdate for a customer. TEST Function
router.put('/customer', function(req, res) {
console.log(req.query.id);
Customer.findById(req.query.id, function(err, p) {
  if (!p)
    return next(new Error('Could not load Document'));
  else {
    // do your updates here
    p.signUpDate = new Date();
    p.save(function(err) {
      if (err)
        console.log('error')
      else
        console.log('success')
    });
  }
});
res.send({response:"Success"});
});

// endpoint which checks if username-password match or not
router.post('/customer/login', function(req, res) {
console.log(req.body);

username = req.body.username;
password = req.body.password;

Customer.findById(username, function(err, p) {
      if (!p){
        console.log('Username not found');
        return res.send({success:false});
        }
//    return next(new Error('Could not load Document'));
      else {
      console.log('Username found');
          if (p.password == password){
            console.log('Password match');
            return res.send({success:true,customer:p});
            }
            else{
                console.log('Password not match');
            return res.send({success:false});
            }
        }
    });
});

// Routes all CRUD operations supported on all models. Internally handled by mongoose.
Customer.methods(['get', 'put', 'post', 'delete']);
Customer.register(router, '/customer');

Product.methods(['get', 'post', 'delete']);
Product.register(router, '/product');

Address.methods(['get', 'put', 'post', 'delete']);
Address.register(router, '/address');

Client.methods(['get', 'put', 'post', 'delete']);
Client.register(router, '/client');

Order.methods(['get', 'put', 'post', 'delete']);
Order.register(router, '/order');

// Return router
module.exports = router;
