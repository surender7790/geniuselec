'use strict';

angular.module('confusionApp', ['ui.router','ngResource','ngDialog'])
.config(function($stateProvider, $urlRouterProvider) {
        $stateProvider

            // route for the home page
            .state('app', {
                url:'/',
                views: {
                    'header': {
                        templateUrl : 'app/views/header.html',
                        controller  : 'HeaderController'
                    },
                    'content': {
                        templateUrl : 'app/views/home.html',
                        controller  : 'HomeController'
                    },
                    'footer': {
                        templateUrl : 'app/views/footer.html',
                    }
                }
            })

            // route for the clientele page
            .state('app.clientele', {
                url: 'clientele',
                views: {
                    'content@': {
                        templateUrl : 'app/views/clientele.html',
                        controller  : 'ClientController'
                    }
                }
            })

            // route for the catalog page
            .state('app.catalog', {
                url: 'catalog',
                views: {
                    'content@': {
                        templateUrl : 'app/views/catalog.html',
                        controller  : 'CatalogController'
                   }
                }
            });

        $urlRouterProvider.otherwise('/');
    })
;
