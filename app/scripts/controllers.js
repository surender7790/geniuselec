'use strict';

angular.module('confusionApp')

.controller('ClientController', ['$scope', '$http','$rootScope', function ($scope, $http, $rootScope) {

}])

.controller('CatalogController', ['$scope', function ($scope) {

   $scope.currentOrder = [];
   $scope.order = {};
   $scope.cartEmpty = true;

   $scope.cartAmount = 0;
   $scope.taxAmount = 0;
   $scope.grandTotal = 0;

   $scope.addProduct = function(quantity,product){
       $scope.order = {
           product:product,
           quantity:quantity,
           price:product.price*quantity
       };
       console.log($scope.order);
       $scope.currentOrder.push($scope.order);



       $scope.cartEmpty = false;

       for (var item in $scope.currentOrder){
            $scope.cartAmount = parseInt($scope.cartAmount + $scope.currentOrder[item].price);
        }

        $scope.taxAmount = parseInt($scope.cartAmount * 0.1);
        $scope.grandTotal = $scope.taxAmount + $scope.cartAmount;

    };
}])

.controller('HomeController', ['$scope', function ($scope) {

}])

.controller('HeaderController', ['$scope', '$state', '$rootScope', 'ngDialog','$http', function ($scope, $state, $rootScope, ngDialog, $http) {

       $scope.loadClients = function(){
            $http.get('http://localhost:3000/api/client').
                success(function(response) {
                    console.log(response);
                    $rootScope.clients = response;
                }).
                error(function() {
                    console.log('error');
                });
        };

       $scope.loadProducts = function(){
            $http.get('http://localhost:3000/api/product').
                success(function(response) {
                    console.log(response);
                    $rootScope.products = response;
                }).
                error(function() {
                    console.log('error');
                });
        };


    $scope.slide = function (dir) {
     console.log(dir);
        $('#carousel-example-generic').carousel(dir);
    };

    $rootScope.loggedIn = false;
    $rootScope.username = '';

    $scope.openLogin = function () {
        ngDialog.open({ template: 'app/views/login.html', scope: $scope, className: 'ngdialog-theme-default', controller:"LoginController" });
    };

    $scope.checkout = function () {
        ngDialog.open({ template: 'app/views/checkout.html', scope: $scope, className: 'ngdialog-theme-default', controller:"CheckoutController" });
    };

    $scope.logOut = function() {
        $rootScope.loggedIn = false;
        $rootScope.username = '';
    };
    
    $rootScope.$on('login:Successful', function () {
    $rootScope.loggedIn = true;
    });
        
    $rootScope.$on('registration:Unsuccessful', function () {
    $rootScope.loggedIn = false;
    });
    
    $scope.stateis = function(curstate) {
       return $state.is(curstate);  
    };
    
}])

.controller('LoginController', ['$scope', 'ngDialog','$http','$rootScope', function ($scope, ngDialog, $http, $rootScope) {

    $scope.firstTime = true;
    $scope.doLogin = function() {
            $scope.firstTime = true;
            var payload = {}
            payload.username = $scope.loginData.username;
            payload.password = $scope.loginData.password;

            console.log(payload);

            $http.post('http://localhost:3000/api/customer/login', payload).
                success(function(response) {
                    console.log(response);
                    if (response.success){
                        $rootScope.username = response.customer.name;
                        $scope.firstTime = false;
                        $rootScope.$broadcast('login:Successful');
                        ngDialog.close();
                        }
                    else{
                        $scope.firstTime = false;
                        $rootScope.$broadcast('login:Unsuccessful');
                        }
                }).
                error(function() {
                    $scope.firstTime = false;
                    $rootScope.$broadcast('login:Unsuccessful');
                    console.log('error');
                });
    };
    $scope.openRegister = function () {
//        ngDialog.close();
        ngDialog.open({ template: 'app/views/register.html', scope: $scope, className: 'ngdialog-theme-default', controller:"RegisterController" });
    };
}])

.controller('RegisterController', ['$scope', 'ngDialog','$http', function ($scope, ngDialog,$http) {

    $scope.firstTime = true;

    $scope.doRegister = function() {
        console.log('Doing registration', $scope.registration);

            console.log($scope.registration);

            $http.post('http://localhost:3000/api/customer', $scope.registration).
                success(function(response) {
                    console.log(response);
                    if (response.__v == 0){
                        $scope.registered = true;
                        $scope.firstTime = false;
                        }
                    else{
                        $scope.registered = false;
                        $scope.firstTime = false;
                        }
                }).
                error(function() {
                        $scope.registered = false;
                        $scope.firstTime = false;
                        console.log('error');
                });
    };
}])

.controller('CheckoutController', ['$scope', 'ngDialog','$http','$rootScope', function ($scope, ngDialog, $http, $rootScope) {

    $scope.firstTime = true;

    $scope.doPay = function() {
    $scope.deliveryData.customer_id = $rootScope.username;
    $scope.deliveryData._id = $rootScope.username;

    console.log($scope.deliveryData);
        $http.post('http://localhost:3000/api/address', $scope.deliveryData).
            success(function(response) {
                console.log(response);
                if (response.__v == 0){
                    $scope.paid = true;
                    $scope.firstTime = false;
                    }
                else{
                    $scope.paid = false;
                    $scope.firstTime = false;
                    }
            }).
            error(function() {
                    $scope.paid = false;
                    $scope.firstTime = false;
                    console.log('error');
            });
    };
}]);